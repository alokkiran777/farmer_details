<!DOCTYPE html>
<html>
 <head>
  <title>Farmers Details</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
 
 <style type="text/css">
 
body{
   
  background-image: url("farm2.jpg");


 </style>
  
 </head>
 <body >
  <div class="container">
    
   <h3 align="center">Farmers Details</h3>
   <div class="well">
   <div ng-app="myapp" ng-controller="formcontroller">
    <form name="userForm" ng-submit="insertData()">
     <label class="text-success" ng-show="successInsert">{{successInsert}}</label>
     <div class="form-group">
      <label>Serial Number <span class="text-danger">*</span></label>
      <input type="number" name="Serial_Number" ng-model="insert.Serial_Number" class="form-control" />
      <span class="text-danger" ng-show="errorSerialNumber">{{errorSerialNumber}}</span>
     </div>
        <hr>
     <div class="form-group">
      <label>Name <span class="text-danger">*</span></label>
      <input type="text" name="Name" ng-model="insert.Name" class="form-control" />
      <span class="text-danger" ng-show="errorName">{{errorName}}</span>
     </div>
     <hr>
       
        <div class="form-group">
      <label>Age<span class="text-danger">*</span></label>
      <input type="number" name="Age" ng-model="insert.Age" class="form-control" />
      <span class="text-danger" ng-show="errorAge">{{errorAge}}</span>
     </div>
        <hr>
        
        
        <div class="form-group">
      <label>Mandal<span class="text-danger">*</span></label>
      <input type="VARCHAR" name="Mandal" ng-model="insert.Mandal" class="form-control" placeholder="i.e. Mandal 1 or Mandal 2 or Mandal 3" />
      <span class="text-danger" ng-show="errorMandal">{{errorMandal}}</span>
     </div>
        <hr>

      <div class="form-group">
      <label>Village<span class="text-danger">*</span></label>
      <input type="VARCHAR" name="Village" ng-model="insert.Village" class="form-control" placeholder="i.e. Village A or Village B ..." />
      <span class="text-danger" ng-show="errorVillage">{{errorVillage}}</span>
     </div>
        <hr>  

      
        
     <div class="form-group">
      <input type="submit" name="insert" class="btn btn-info" value="Submit"/>
     </div>
    </form>
   </div>
 </div>
  </div>
 </body>
</html>



<script>
var application = angular.module("myapp", []);
application.controller("formcontroller", function($scope, $http){
 $scope.insert = {};
 $scope.insertData = function(){
  $http({
   method:"POST",
   url:"insert.php",
   data:$scope.insert,
  }).success(function(data){
   if(data.error)
   {
    $scope.errorSerialNumber = data.error.Serial_Number;
    $scope.errorName = data.error.Name;
       $scope.errorAge = data.error.Age;
       $scope.errorMandal = data.error.Mandal;
     $scope.errorVillage = data.error.Village;
    $scope.successInsert = null;
   }
   else
   {
    $scope.insert = null;
    $scope.errorSerialNumber = null;
    $scope.errorName = null;
       $scope.errorAge = null;
       $scope.errorMandal = null;
              $scope.errorVillage = null;
    $scope.successInsert = data.message;
   }
  });
 }
});
</script>
